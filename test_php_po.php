<?php

$time_start = microtime(true);

$locale = "en";
putenv("LC_MESSAGES=$locale");

bindtextdomain("default4", ".");
textdomain("default4");
$loop = 100000;
for ($i=0; $i<$loop; $i++){
	echo _('test40')."\n";
	echo _('test88')."\n";
}

$time_end = microtime(true);
$time = $time_end - $time_start;

echo "Terminado en $time segundos, ".(round(($loop*2)/$time))." peticiones por segundo\n";
echo 'Cantidad de memoria usada: '.memory_get_usage() . "\n";