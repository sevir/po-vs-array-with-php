<?php
$time_start = microtime(true);

include_once './es_ES/lang_test4.php';

$loop = 100000;
for ($i=0; $i<$loop; $i++){
	echo $lang['test40']."\n";
	echo $lang['test88']."\n";
}

$time_end = microtime(true);
$time = $time_end - $time_start;

echo "Terminado en $time segundos, ".(round(($loop*2)/$time))." peticiones por segundo\n";
echo 'Cantidad de memoria usada: '.memory_get_usage() . "\n";